import './HomePage.scss';
import { useNavigate } from 'react-router-dom';

const HomePage = () => {
  const navigate = useNavigate();
  return (
    <>
      <h1 className="title">Blog</h1>
      <div className="separator" />
      <button className="cta" onClick={() => navigate('/news')}>
        Go to articles
      </button>
      <p>
        Lorem ipsum dolor sit amet. A totam debitis et architecto obcaecati ut
        beatae nobis et perferendis quia et reiciendis temporibus sit explicabo
        quidem. Aut debitis facilis hic laudantium provident hic minima ipsum
        est omnis iusto sit alias aliquam et nobis excepturi qui consequatur
        mollitia. Non harum corporis ut ipsam distinctio sed corporis
        consequuntur!
      </p>
    </>
  );
};

export default HomePage;
