import './Article.scss';

const Article = ({ urlToImage, url, author, title }) => {
  return (
    <a href={url} className="article__wrapper">
      <img src={urlToImage} alt="urlToImage" className="article__image" />
      <strong className="article__title">{title}</strong>
      <em>{author}</em>
    </a>
  );
};

export default Article;
