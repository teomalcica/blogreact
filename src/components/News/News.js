import './News.scss';
import { useState, useEffect } from 'react';
import { useParams, useNavigate } from 'react-router-dom';
import useNews from '../../hook/useNews';
import Article from '../article/Article';

const News = () => {
  const { country } = useParams();
  const navigate = useNavigate();
  const [currentTab, setCurrentTab] = useState(
    country ? country.toUpperCase() : 'RO'
  );

  const articles = useNews();

  const countries = {
    RO: 'Romania',
    US: 'USA',
    GB: 'UK',
    AU: 'Australia',
  };

  useEffect(() => {
    if (!country) {
      navigate('/news/ro');
      return;
    }
    if (!countries[country.toUpperCase()]) {
      console.log(country);
      navigate('/404');
    }
  }, [country, navigate]);

  return (
    <>
      <h1>News</h1>
      <div className="tabs__wrapper">
        {Object.keys(countries).map(key => (
          <div
            className={currentTab === key ? 'tab tab--selected' : 'tab'}
            key={key}
            onClick={() => {
              setCurrentTab(key);
              navigate(`/news/${key}`);
            }}
          >
            {countries[key]}
          </div>
        ))}
      </div>
      <div className="tab-content__wrapper">
        {articles.map(article => (
          <Article
            urlToImage={article.urlToImage}
            url={article.url}
            author={article.author}
            title={article.title}
          />
        ))}
      </div>
    </>
  );
};

export default News;
