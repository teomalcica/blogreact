import { useState, useEffect } from 'react';
import { useParams } from 'react-router-dom';

const useNews = () => {
  const [currentArticles, setCurrentArticles] = useState({});
  const { country } = useParams();
  const url = `https://newsapi.org/v2/top-headlines?country=${country}&apiKey=f19fb123777c45c2bb037eab020bb732`;

  useEffect(() => {
    const fetchData = async () => {
      const res = await fetch(url);
      const data = await res.json();
      setCurrentArticles({ ...currentArticles, [country]: data.articles });
    };
    fetchData();
  }, [country]);
  return currentArticles[country] || [];
};

export default useNews;
