import './App.scss';
import HomePage from './components/homepage/HomePage';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import News from './components/news/News';

function App() {
  return (
    <div className="wrapper">
      {/* <HomePage /> */}
      {/* <News /> */}
      <Router>
        <Routes>
          <Route path="/" element={<HomePage />}></Route>
          <Route path="/news" element={<News />}></Route>
          <Route path="/news/:country" element={<News />}></Route>
          <Route path="*" element={<div>Mai incearca :(</div>}></Route>
        </Routes>
      </Router>
    </div>
  );
}

export default App;
